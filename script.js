class Cache {
    constructor() {
        this.state = {};
    }

    setState(key, value) {
        this.state[key] = value;
    }
}

class DataManagement {

    async getData(currentPage) {
        return await fetch(`https://swapi.dev/api/planets?page=${currentPage}`)
            .then(res => res.json())
    }

    async loadDetails(urlArr, func) {
        Promise.all(urlArr)
            .then(responses => Promise.all(responses.map(r => r.json())))
            .then(planets => func(planets));
    }
}

class ApplicationState {
    constructor() {
        this.planetsCache = new Cache();
        this.dataMahagement = new DataManagement();
        this.page = new MainContent();
        this.modal = new Modal();
        this.pagination = new Pagination();
        this.numOfPages = 1;
        this.pageContainer = document.querySelector(`.container`);
    }

    async getPlanets(size = 10, currentPage = 1) {
        if (!this.planetsCache.state[currentPage]) {
            this.page.renderSpinner();
            await this.dataMahagement.getData(currentPage)
                .then(res => ({
                    results: res.results,
                    totalCount: +res.count,
                    numOfPages: Math.ceil(+res.count / size)
                }))
                .then(({results, numOfPages}) => {
                    this.numOfPages = numOfPages;
                    this.planetsCache.setState(currentPage, results);
                    this.page.clearPage();
                    this.page.renderPosts(results);
                    this.pagination.setTotal(numOfPages)
                    this.pagination.render();
                })
        } else {
            this.page.clearPage();
            this.page.renderPosts(this.planetsCache.state[currentPage]);
        }
    }

    async showAll() {
        this.page.renderSpinner();
        let resultArray = [];
        for (let i = 1; i <= this.numOfPages; i++) {
            if (!this.planetsCache.state[i]) {
                await this.dataMahagement.getData(i)
                    .then(res => {
                        this.planetsCache.setState(i, res.results);
                        resultArray = [...resultArray, ...res.results];

                    });
            } else {
                resultArray = [...resultArray, ...this.planetsCache.state[i]];
            }
        }
        this.page.clearPage();
        this.page.renderPosts(resultArray);
        this.pagination.deActiveButtons();
    }

    handlePage() {
        this.pageContainer.addEventListener(`click`,
            event => {
                event.preventDefault();
                const target = event.target;
                if (target.classList.contains(`js-modal`)) {
                    this.modal.handleEvent(this.dataMahagement.loadDetails, target)
                } else if (target.classList.contains(`js-pagination`)) {
                    this.pagination.handleEvent(target);
                    if (this.pagination.pageChange) {
                        this.getPlanets(10, this.pagination.getCurrentPage());
                    }
                } else if (target.classList.contains(`js-show-all`)) {
                    this.showAll();
                }
            })
    }

    start() {
        this.getPlanets();
        this.handlePage();
    }

}

class MainContent {
    constructor() {
        this.postsContainer = document.querySelector(`.planets__container`);
    }

    renderPosts(array) {
        array.forEach(item => {
            this.postsContainer.innerHTML +=
                `<div class="card" style="width: 18rem;">
                    <div class="card-body d-flex flex-column">
                        <h5 class="card-title">${item.name}</h5>
                        <p class="card-text">Diameter: ${item.diameter}</p>
                        <p class="card-text">Population: ${item.population}</p>
                        <p class="card-text">Gravity: ${item.gravity}</p>
                        <p class="card-text">Terrain: ${item.terrain}</p>
                        <p class="card-text">Climate: ${item.climate}</p>
                        <a href="#" class="btn btn-primary mt-auto js-open-modal js-modal" data-bs-toggle="modal" data-bs-target="#exampleModal" planet-name="${item.name.toLowerCase()}">Detail</a>
                    </div>
                </div>`
        })
    }

    renderSpinner() {
        this.clearPage();
        this.postsContainer.innerHTML =
            `<div class="spinner-border" role="status">
                <span class="visually-hidden">Loading...</span>
            </div>`
    }

    clearPage() {
        this.postsContainer.innerHTML = ``;
    }
}

class Modal {
    constructor() {
        this.modal = document.querySelector(`.modal`);
    }

    async handleEvent(loadDetails, target) {

        const planetName = target.getAttribute(`planet-name`);
        this.modalSpinner();
        const planet = await fetch(`https://swapi.dev/api/planets/?search=${planetName}`)
            .then(res => res.json())
            .then(planet => planet.results[0])
        const planetsUrls = await planet.films;
        const planetsRequest = await planetsUrls.map(url => fetch(url));
        const charactersUrls = await planet.residents;
        const charactersRequest = await charactersUrls.map(url => fetch(url));
        this.renderPlanetInModal(planet);
        loadDetails(planetsRequest, this.renderMovies);
        loadDetails(charactersRequest, this.renderCharacters);


    }

    renderPlanetInModal(planet) {
        this.modal.innerHTML =
            `<div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">${planet.name}</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <p>Diametr: ${planet.diameter}</p>
                    <p>Population: ${planet.population}</p>
                    <p>Gravity: ${planet.gravity}</p>
                    <p>Terrain: ${planet.terrain}</p>
                    <p>Climate: ${planet.climate}</p>
                    <div class="movies__list">
                        <div class="spinner-border" role="status">
                            <span class="visually-hidden">Loading...</span>
                        </div>
                    </div>
                    <div class="characters__list">
                    </div>
                </div>
                
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                </div>
            </div>
        </div>`
    }

    modalSpinner() {
        this.modal.innerHTML =
            `<div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Loading...</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <div class="spinner-border" role="status">
                            <span class="visually-hidden">Loading...</span>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>`
    }

    renderMovies(objArr) {
        const movieList = document.querySelector(`.movies__list`);
        movieList.innerHTML = `<h4>Movies List:</h4>`;
        objArr.map(movie => movieList.innerHTML += `
            <h5>Episode: ${movie.episode_id}</h5>
            <h6>Title: ${movie.title}</h6>
            <div>Release date: ${movie.release_date}</div>
            <hr>
            `);
    }

    renderCharacters(objArr) {
        const charactersList = document.querySelector(`.characters__list`);
        charactersList.innerHTML = `<h4>Characters List:</h4>`;
        objArr.map(async character => {
            const homeworld = await fetch(character.homeworld)
                .then(res => res.json())
            charactersList.innerHTML += `
            <h6>Name: ${character.name}</h6>
            <div>Gender: ${character.gender}</div>
            <div>Birth Year: ${character.birth_year}</div>
            <div>Homeworld: ${homeworld.name}</div>
            <hr>
            `
        });
    }
}

class Pagination {
    constructor() {
        this.paginator = document.querySelector(`.pagination`);
        this.currentPage = 1;
        this.total = 0;
        this.pageChange = false;
    }

    handleEvent(target) {
        if (target.classList.contains(`disabled`)) {
            this.pageChange = false;
            return
        }
        if (target.classList.contains(`link`)) {
            this.currentPage = +target.getAttribute(`link-id`);
        } else if (target.classList.contains(`previous`)) {
            this.currentPage--;
        } else if (target.classList.contains(`next`)) {
            this.currentPage++;
        }
        this.pageChange = true;
        this.render(this.total, this.currentPage);
    }

    deActiveButtons() {
        this.paginator.querySelector(`.active`).classList.remove(`active`);
    }

    render() {
        this.paginator.innerHTML =
            `<li class="page-item ${this.currentPage === 1 && "disabled"}">
                <a class="page-link previous js-pagination" href="#">Previous</a>
            </li>`
        for (let i = 1; i <= this.total; i++) {
            this.paginator.innerHTML += `<li class="page-item ${this.currentPage === i && `active`}"><a class="page-link link js-pagination" href="#" link-id="${i}">${i}</a></li>`
        }
        this.paginator.innerHTML +=
            `<li class="page-item ${this.currentPage === this.total && "disabled"}">
                <a class="page-link next js-pagination" href="#">Next</a>
            </li>`
    }

    setTotal(num) {
        this.total = num;
    }

    getCurrentPage() {
        return this.currentPage;
    }
}

const app = new ApplicationState();
app.start();